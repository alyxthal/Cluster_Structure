#!/usr/bin/python
import re
import os
import sys
import numpy as np
from math import cos, sin, sqrt
from sklearn import svm
import itertools
from numpy import genfromtxt
import pytest
#Individual atom with the neighbours and angles
class Atom(object):
    def __init__(self,id,coord,neighbours,angles):
        self.id=id
        self.coord=coord
        self.neighbours=neighbours
        self.angles=angles



#Read structure from lammpstrj file
def readFromLammpstrj(filename,atoms):
    fin=open(filename,'r')
    fin.readline()
    fin.readline()
    fin.readline()
    tot_atom=int(fin.readline().strip())
    fin.readline()
    fin.readline()
    fin.readline()
    fin.readline()
    fin.readline()
    for i in range(tot_atom):
        #(id,atype,x,y,z,vx,vy,vz,q)=fin.readline().strip().split(" ",9)
        (id,atype,x,y,z,q)=fin.readline().strip().split(" ",6)
        atoms.append(Atom(int(id),[float(x),float(y),float(z)],[],[]))
    fin.close()
    return atoms

#Read structure  from xyz file
def readFromXYZ(filename,atoms):
    fin=open(filename,'r')
    tot_atom=int(fin.readline().strip())
    fin.readline()
    for i in range(tot_atom):
        (el,x,y,z)=fin.readline().strip().split(" ",4)
        atoms.append(Atom(i+1,[float(x),float(y),float(z)],[],[]))
    fin.close()
    return atoms



#adds ids of nearest neighbours to each atoms
def findNearestNeighbours(atoms,cutoff):
    for (i,j) in itertools.product(atoms,atoms):
        r1=i.coord
        r2=j.coord
        vect=sqrt((r1[0]-r2[0])**2+(r1[1]-r2[1])**2+(r1[2]-r2[2])**2)
        if (vect<cutoff) and (vect>0.1):
            atoms[i.id-1].neighbours.append(j.id-1)


#Returns angle between two vectros in degrees
def angleBetweenVectors(x,y):
    dot=np.dot(x,y)
    x_modulus = np.sqrt((x*x).sum())
    y_modulus = np.sqrt((y*y).sum())
    cos_angle = dot/x_modulus/y_modulus
    return np.arccos(cos_angle)*360/2/np.pi



def calcAngles(atoms):
    for i in range(len(atoms)):
        for j,k in zip(atoms[i].neighbours, itertools.cycle(atoms[i].neighbours)):
            r1=atoms[i].coord
            r2=atoms[j].coord
            r3=atoms[k].coord
            r4=np.subtract(r2,r1)
            r5=np.subtract(r3,r4)
            angle=angleBetweenVectors(r4,r5)
            atoms[i].angles.append(angle)



#Goes through  atoms and angles and calculates histogram of all angles in [0,Pi] range
def createHist(atoms):
    x=[i*5 for i in range(37)]
    y=[0]*37
    for angle in [(angle) for atom in atoms for angle in atom.angles]:
        y[int(angle//5)]+=1
    y_norm=map(lambda x: float(x)/float(max(y)),y)
    return x,y_norm

#Writes histogram to a fine
def writeToFile(filename, hist,type):
    fout=open(filename,'a')
    fout.write(" ".join('%.4f' %x for x in hist))
    fout.write(" %i" %type)
    fout.write("\n")
    fout.close()

#run SVM learning algorith on training set
def learnStructure(hist):
    hist=list(hist)
    data = genfromtxt('database', delimiter=' ')
    X=np.delete(data, np.s_[37,38],  1)
    Y=data[:,37]
    print(X)
    clf= svm.SVC(gamma=0.001, C=1000, probability=True)
    clf.fit(X,Y)
    return  clf.predict_proba(hist)

def determineMorphology(filename):
    atoms=[]
    atoms=readFromXYZ(filename,atoms)
    #atoms=readFromLammpstrj(filename,atoms)
    findNearestNeighbours(atoms,3)
    calcAngles(atoms)
    x_hist,y_hist=createHist(atoms)
    result=str(learnStructure(y_hist))
    output=re.findall('-?\ *[0-9]+\.?[0-9]*(?:[Ee]\ *-?\ *[0-9]+)?', result)
    Oh,Dh,Ih=float(output[0]),float(output[1]),float(output[2])
    return Oh,Dh,Ih

if __name__== "__main__":
    filename=sys.argv[1]
    Oh,Dh,Ih=determineMorphology(filename)
    print('It is TOh: %f Dh: %f Ih: %f' %(Oh,Dh,Ih))
